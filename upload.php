<?php

function send_mail_with_attachment($to, $subject, $message, $additional_headers, array $attachments)
{
	$fout = popen('sendmail ' . escapeshellarg($to), 'w');

	if (!$fout)
		throw new Exception("Could not open sendmail");

	$boundary = md5(microtime());

	// Headers and dummy message
	fwrite($fout,
		"MIME-Version: 1.0\r\n"
		. ($additional_headers ? (trim($additional_headers, "\r\n") . "\r\n") : "")
		. "Subject: $subject\r\n"
		. "Content-Type: multipart/mixed; boundary=\"$boundary\"\r\n"
		. "\r\n"
		. "This is a mime-encoded message"
		. "\r\n\r\n");

	// Message content
	fwrite($fout, "--$boundary\r\n"
		. "Content-Type: text/plain; charset=\"UTF-8\"\r\n"
		. "Content-Transfer-Encoding: quoted-printable\r\n\r\n");

	$filter = stream_filter_append($fout, 'convert.quoted-printable-encode');

	if (is_resource($message))
		stream_copy_to_stream($message, $fout);
	else
		fwrite($fout, $message);

	stream_filter_remove($filter);

	fwrite($fout, "\r\n");

	foreach ($attachments as $file_name => $file)
	{
		$file_handle = is_resource($file) ? $file : fopen($file, 'rb');
		// Attachment
		fwrite($fout, "\r\n--$boundary\r\n"
			. "Content-Type: application/octet-stream; name=\"" . addslashes($file_name) . "\"\r\n"
			. "Content-Transfer-Encoding: base64\r\n"
			. "Content-Disposition: attachment\r\n\r\n");

		$filter = stream_filter_append($fout, 'convert.base64-encode', STREAM_FILTER_WRITE, [
			"line-length" => 80,
			"line-break-chars" => "\r\n"]);

		stream_copy_to_stream($file_handle, $fout);

		stream_filter_remove($filter);

		fclose($file_handle);
	}

	fwrite($fout, "\r\n--$boundary--\r\n");

	fclose($fout);
}

function handle_upload()
{
	$name = filter_input(INPUT_POST, 'name');

	$email = filter_input(INPUT_POST, 'email', FILTER_VALIDATE_EMAIL);

	$message = filter_input(INPUT_POST, 'message');

	if (!$name)
		throw new Exception("No name given");

	if (!$email)
		throw new Exception("No email address given");

	if (!isset($_FILES['file']))
		throw new Exception("No file uploaded");

	if (count(array_diff($_FILES['file']['error'], [0])))
		throw new Exception("Error during upload");

	$attachments = array_combine($_FILES['file']['name'], $_FILES['file']['tmp_name']);

	send_mail_with_attachment(
		"studcee@svcover.nl",
		"Submitted support material",
		$message,
		"From: $name <$email>",
		$attachments);
}

$error = null;

if ($_SERVER['REQUEST_METHOD'] == 'POST') {
	try {
		handle_upload();
		header('Location: index.php?submitted=ok');
		exit;
	} catch (Exception $e) {
		$error = $e->getMessage();
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<title>Upload new material</title>
		<style>
			* {
				box-sizing: border-box;
			}

			body, input, textarea, button {
				font: 16px/20px sans-serif;
			}

			input[type=text], input[type=email], textarea, button {
				border: 1px solid #ccc;
				padding: 4px;
				width: 100%;
			}

			button {
				cursor: pointer;
				background: #ccc;
				border: 0;
				padding: 8px;
			}

			button:hover, button:focus {
				background: #ddd;
			}

			form {
				display: block;
				max-width: 300px;
				margin: 100px auto 0 auto;
			}

			form > div {
				margin-bottom: 20px;
			}

			form > div > label {
				display: block;
			}

			form > div > label:after {
				content: ':';
			}
	
			.error {
				background: #c00;
				color: #fff;
				padding: 4px;
			}
		</style>
	</head>
	<body>
		<form method="post" enctype="multipart/form-data">
			<?php if ($error): ?>
			<p class="error"><?=$error?></p>
			<?php endif ?>

			<div>
				<label for="name-field">Your name</label>
				<input type="text" id="name-field" name="name">
			</div>

			<div>
				<label for="email-field">Your email</label>
				<input type="email" id="email-field" name="email">
			</div>

			<div>
				<label for="file-field">Attachments</label>
				<input type="file" id="file-field" name="file[]" multiple>
				<em>Please include the sources if possible.</em>
			</div>

			<div>
				<label for="message-field">Remarks</label>
				<textarea name="message" id="message-field" rows="5" placeholder="E.g. which course, which year in case of older material"></textarea>
			</div>
			
			<div class="controls">
				<button type="submit">Submit</button>
			</div>
		</form>
	</body>
</html>